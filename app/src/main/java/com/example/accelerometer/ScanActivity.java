package com.example.accelerometer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ScanActivity extends AppCompatActivity implements SensorEventListener {
    private String label;

    private LineGraphSeries<DataPoint> xAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
        new DataPoint(0, 0)
    });
    private LineGraphSeries<DataPoint> yAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
        new DataPoint(0, 0)
    });;
    private LineGraphSeries<DataPoint> zAccSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
        new DataPoint(0, 0)
    });;

    private LineGraphSeries<DataPoint> xGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });
    private LineGraphSeries<DataPoint> yGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;
    private LineGraphSeries<DataPoint> zGyroSeries = new LineGraphSeries<DataPoint>(new DataPoint[]{
            new DataPoint(0, 0)
    });;

    private SensorManager mSensorManager;

    private Sensor mAccelerometer;
    private Sensor mGyroscope;

    private long graphAccLastXValue = 1;
    private long graphGyroLastXValue = 1;
    private int savedWindows = 0;

    private Button stop;
    private TextView savedWindowsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        initAccelerometer();
        initGyroscope();

        Intent intent = getIntent();
        this.label = intent.getStringExtra(MainActivity.EXTRA_LABEL);

        TextView textView = findViewById(R.id.label);
        textView.setText(label);

        savedWindowsTextView = findViewById(R.id.saved_windows);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        // register sensor listeners
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);

        stop = findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initAccelerometer() {
        xAccSeries.setColor(Color.RED);
        yAccSeries.setColor(Color.GREEN);
        zAccSeries.setColor(Color.BLUE);

        GraphView accGraph = (GraphView) findViewById(R.id.acc_graph);

        accGraph.addSeries(xAccSeries);
        accGraph.addSeries(yAccSeries);
        accGraph.addSeries(zAccSeries);

        Viewport accViewport = accGraph.getViewport();

        accViewport.setXAxisBoundsManual(true);
        accViewport.setMaxX(10);
        accViewport.setMinX(-10);

        accGraph.getViewport().setYAxisBoundsManual(true);
        accViewport.setMaxY(10);
        accViewport.setMinY(-10);

        accViewport.setScrollable(true);
        accGraph.getViewport().setScalableY(true);
        accGraph.getViewport().setScrollableY(true);
    }

    private void initGyroscope() {
        xGyroSeries.setColor(Color.RED);
        yGyroSeries.setColor(Color.GREEN);
        zGyroSeries.setColor(Color.BLUE);

        GraphView gyroGraph = (GraphView) findViewById(R.id.gyro_graph);

        gyroGraph.addSeries(xGyroSeries);
        gyroGraph.addSeries(yGyroSeries);
        gyroGraph.addSeries(zGyroSeries);

        Viewport gyroViewport = gyroGraph.getViewport();

        gyroViewport.setXAxisBoundsManual(true);
        gyroViewport.setMaxX(10);
        gyroViewport.setMinX(-10);

        gyroGraph.getViewport().setYAxisBoundsManual(true);
        gyroViewport.setMaxY(10);
        gyroViewport.setMinY(-10);

        gyroViewport.setScrollable(true);
        gyroGraph.getViewport().setScalableY(true);
        gyroGraph.getViewport().setScrollableY(true);
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void stopScan() {
        if (graphAccLastXValue % 128 == 0) {
            mSensorManager.unregisterListener(this, mAccelerometer);
        }
        if (graphGyroLastXValue % 128 == 0) {
            mSensorManager.unregisterListener(this, mGyroscope);
        }
        if (graphAccLastXValue % 128 != 0 || graphGyroLastXValue % 128 != 0) {
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Iterator<DataPoint> xAcc = xAccSeries.getValues(0, graphAccLastXValue);
        Iterator<DataPoint> yAcc = yAccSeries.getValues(0, graphAccLastXValue);
        Iterator<DataPoint> zAcc = zAccSeries.getValues(0, graphAccLastXValue);

        Iterator<DataPoint> xGyro = xGyroSeries.getValues(0, graphGyroLastXValue);
        Iterator<DataPoint> yGyro = yGyroSeries.getValues(0, graphGyroLastXValue);
        Iterator<DataPoint> zGyro = zGyroSeries.getValues(0, graphGyroLastXValue);

        String xJson = new Gson().toJson(this.mapIntoArrayList(xAcc));
        String yJson = new Gson().toJson(this.mapIntoArrayList(yAcc));
        String zJson = new Gson().toJson(this.mapIntoArrayList(zAcc));

        Map<String, String> coordinates = new HashMap<>();
        coordinates.put("x", xJson);
        coordinates.put("y", yJson);
        coordinates.put("z", zJson);
        coordinates.put("label", this.label);

        final SensorEventListener context = this;

        ++savedWindows;
       savedWindowsTextView.setText(Integer.toString(savedWindows));

        // Add a new document with a generated ID
        db.collection("accelerometer_data")
            .add(coordinates)
            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    Log.d("DEBUG", "DocumentSnapshot added with ID: " + documentReference.getId());

                    xAccSeries.resetData(new DataPoint[]{
                            new DataPoint(0, 0)
                    });
                    yAccSeries.resetData(new DataPoint[]{
                            new DataPoint(0, 0)
                    });
                    zAccSeries.resetData(new DataPoint[]{
                            new DataPoint(0, 0)
                    });
                    mSensorManager.registerListener(context, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w("DEBUG", "Error adding document", e);
                }
            });

        xJson = new Gson().toJson(this.mapIntoArrayList(xGyro));
        yJson = new Gson().toJson(this.mapIntoArrayList(yGyro));
        zJson = new Gson().toJson(this.mapIntoArrayList(zGyro));

        coordinates = new HashMap<>();
        coordinates.put("x", xJson);
        coordinates.put("y", yJson);
        coordinates.put("z", zJson);
        coordinates.put("label", this.label);

        db.collection("gyroscope_data")
                .add(coordinates)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("DEBUG", "DocumentSnapshot added with ID: " + documentReference.getId());

                        xGyroSeries.resetData(new DataPoint[]{
                                new DataPoint(0, 0)
                        });
                        yGyroSeries.resetData(new DataPoint[]{
                                new DataPoint(0, 0)
                        });
                        zGyroSeries.resetData(new DataPoint[]{
                                new DataPoint(0, 0)
                        });

                        mSensorManager.registerListener(context, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DEBUG", "Error adding document", e);
                    }
                });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch(event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                displayAccValuesInChart(event);
                break;

            case Sensor.TYPE_GYROSCOPE:
                displayGyroValuesInChart(event);
                break;
        }
    }

    private ArrayList<DataPoint> mapIntoArrayList(Iterator<DataPoint> data) {
        ArrayList<DataPoint> items = new ArrayList();

        while(data.hasNext()) {
            DataPoint element = data.next();
            items.add(element);
        }
        return items;
    }

    private void displayAccValuesInChart(SensorEvent event) {
        TextView tvX= (TextView)findViewById(R.id.acc_x_axis);
        TextView tvY= (TextView)findViewById(R.id.acc_y_axis);
        TextView tvZ= (TextView)findViewById(R.id.acc_z_axis);

        // Remove the gravity contribution with the high-pass filter.
        final float x = event.values[0];
        final float y = event.values[1];
        final float z = event.values[2];

        graphAccLastXValue++;

        tvX.setText(Float.toString(x));
        tvY.setText(Float.toString(y));
        tvZ.setText(Float.toString(z));

        xAccSeries.appendData(new DataPoint(graphAccLastXValue, x), true, 10000);
        yAccSeries.appendData(new DataPoint(graphAccLastXValue, y), true, 10000);
        zAccSeries.appendData(new DataPoint(graphAccLastXValue, z), true, 10000);

        if (graphAccLastXValue % 128 == 0) {
            stopScan();
        }
        //mAccHandler.postDelayed(mAccTimer, 10);
    }

    private void displayGyroValuesInChart(SensorEvent event) {
        TextView tvX = (TextView) findViewById(R.id.gyro_x_axis);
        TextView tvY = (TextView) findViewById(R.id.gyro_y_axis);
        TextView tvZ = (TextView) findViewById(R.id.gyro_z_axis);

        final float x = event.values[0];
        final float y = event.values[1];
        final float z = event.values[2];

        graphGyroLastXValue++;

        tvX.setText(Float.toString(x));
        tvY.setText(Float.toString(y));
        tvZ.setText(Float.toString(z));

        xGyroSeries.appendData(new DataPoint(graphGyroLastXValue, x), true, 10000);
        yGyroSeries.appendData(new DataPoint(graphGyroLastXValue, y), true, 10000);
        zGyroSeries.appendData(new DataPoint(graphGyroLastXValue, z), true, 10000);

        if (graphGyroLastXValue % 128 == 0) {
            stopScan();
        }
    }
}
