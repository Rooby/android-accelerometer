package com.example.accelerometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {
    private EditText mLabel;
    private Button start;

    public static final String EXTRA_LABEL = "LABEL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLabel = findViewById(R.id.label);
        start = findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scan();
            }
        });
    }

    public void scan() {
        String label = mLabel.getText().toString();

        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(EXTRA_LABEL, label);
        startActivity(intent);
    }


}
